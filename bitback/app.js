/*
utils
*/
//For todays date;
Date.prototype.today = function(){ 
    return (((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) + "/" + ((this.getDate() < 10)?"0":"") + this.getDate() + "/"+ this.getFullYear() 
};
//For the time now
Date.prototype.timeNow = function(){
     return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
};


var Bitcoin = Backbone.Model.extend({});

var BitcoinView = Backbone.View.extend({
	tagName: 'div',
	className: 'bitcoin',

	render: function(){
		var template = $("#btc-template").html();
		this.$el.html(_.template(template, this.model.attributes));
		return this;
	}
});

var bitcoin = new Bitcoin({
	bid: 'Loading', 
	ask: 'Loading', 
	last: 'Loading', 
	updated: '--'
});

var bitCoinView = new BitcoinView({model: bitcoin, el: '#bitcoin'});

var ref = new Firebase("https://publicdata-bitcoin.firebaseio.com/");
ref.child("bid").on("value", setPrice);
ref.child("ask").on("value", setPrice);
ref.child("last").on("value", setPrice);

function setPrice(snapshot) {
	console.log(snapshot.name() + ": " + snapshot.val());
	bitcoin.set(snapshot.name(), snapshot.val());
	var newDate = new Date();
	var datetime = newDate.today() + " @ " + newDate.timeNow();
	bitcoin.set('updated', datetime);
	bitCoinView.render();
}

var Stock = Backbone.Model.extend({});

var StockView = Backbone.View.extend({
	tagName: 'div',
	className: 'stock',

	/*initialize: function(){
		_.bindAll(this, 'render');
		this.model.bind('change', this.render);
	},*/

	render: function(){
		this.$el.html(_.template(this.options.template, this.model.attributes));
		return this;
	}
});

var gold = new Stock({price: 'Loading', ticker: 'GLD'});
var silver = new Stock({price: 'Loading', ticker: 'SLV'});
var goldView = new StockView({model: gold, el: '#gold', template: $("#gold-template").html()});
var silverView = new StockView({model: silver, el: '#silver', template: $("#silver-template").html()});

getStocks();
window.setInterval(getStocks, 60000);

function getStocks (){
	var silverAndGoldurl = 'http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quote%20where%20symbol%20in%20(%22GLD%22%2C%20%22SLV%22)&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback='
	$.get(silverAndGoldurl, function(data){
		gold.set('price', data.query.results.quote[0].LastTradePriceOnly);
		silver.set('price', data.query.results.quote[1].LastTradePriceOnly);

		var newDate = new Date();
		var datetime = newDate.today() + " @ " + newDate.timeNow();
		gold.set('updated', datetime);
		silver.set('updated', datetime);
		
		goldView.render();
		silverView.render();
	});
};

//window.setInterval(getStocks, 2000);
/*
_(myModels).each(function(model){
	$('#canvas').append(new RectangleView({model: model}).render().el);		
});


	var Rectangle = Backbone.Model.extend({});

	var RectangleView = Backbone.View.extend({
		tagName: 'div',
		className: 'rectangle',

		render: function(){
			this.setDimensions();
			this.setPosition();
			this.setColor();
			return this;
		},

		events: {
			'click': 'move'
		},

		setDimensions: function(){
			this.$el.css({
				width: this.model.get('width') + 'px', 
				height: this.model.get('height') + 'px'
			});
		},

		setColor: function() {
			this.$el.css('background', this.model.get('color'));
		},

		setPosition: function(){
			var position = this.model.get('position');
			this.$el.css({
				left: position.x,
				top: position.y
			});
		},

		move: function(){
			console.log('move!', this.$el.position().left);
			this.$el.css('left', this.$el.position().left + 10);
		}

	});

	var myModels = [
		new Rectangle({
		width: 100,
		height: 60,
		color: 'red',
		position: {
			x: 300,
			y: 150
		}}
		),
		new Rectangle({
		width: 40,
		height: 60,
		color: 'black',
		position: {
			x: 500,
			y: 50
		}})
	];

	_(myModels).each(function(model){
		$('#canvas').append(new RectangleView({model: model}).render().el);		
	});
	
*/


	

